import React from 'react';
import { useSelector, useDispatch } from "react-redux";
import './App.css';
import ActionType from "./Redux/Actions/ActionType";
import GroceryListTable from "./Components/GroceryListTable";
import GroceryItemTable from "./Components/GroceryItemTable";
import NewGroceryListInput from "./Components/NewGroceryListInput";
import NewGroceryItemInput from "./Components/NewGroceryItemInput";
import Axios from "axios";

function App() {
  const myState = useSelector((state) => state);
  const dispatch = useDispatch();

  console.log(myState);

  if (!myState.initialized) {
    Axios.get("http://localhost:8080/grocery-list")
      .then((result) => {
        dispatch({type: ActionType.INITIALIZE, payload: result.data});
      })
      .catch((err) => console.log("Error getting info from database"));

    return (
      <h1>Loading initial values from database</h1>
    )
  } else {
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-5">
              <GroceryListTable />
              <NewGroceryListInput />
            </div>
            <div className="col-2">
            </div>
            <div className="col-5">
              <GroceryItemTable />
              {(myState.openedList ? <NewGroceryItemInput /> : <React.Fragment />)}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
