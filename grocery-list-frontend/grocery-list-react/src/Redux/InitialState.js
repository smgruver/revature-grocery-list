import GroceryList from "../Model/GroceryList";
import GroceryItem from "../Model/GroceryItem";

/**
 * The intitial state of the application
 */
const InitialState = {
//  count: 0,       //from the YouTube demo I learned redux from
    groceryLists: null,
    openedList: null,
    initialized: false,
    newGroceryListNameInput: "",
    newGroceryItemInput: {
        itemName: null,
        itemPrice: null,
        itemType: {
            typeId: null,
            typeName: null,
        }
    },
    waitingForServer: false,
};

export default InitialState;