import groceryReducer from "./Reducers/groceryReducer";
import { createStore } from "redux";

/**
 * The store for this application's redux architecture.
 */
const store = createStore(groceryReducer);

export default store;