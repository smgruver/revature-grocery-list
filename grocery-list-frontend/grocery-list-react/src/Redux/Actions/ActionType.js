/**
 * A const, frozen object to serve as an enum for the various valid types of actions.
 */
const ActionType = {
    SELECT_LIST: 3,
    INITIALIZE: 10,
    INPUT_GROCERY_LIST_NAME: 11,
    SUBMIT_NEW_GROCERY_LIST: 12,
    RECEIVE_NEW_GROCERY_LIST: 13,
    INPUT_GROCERY_ITEM_INFO: 14,
    SUBMIT_NEW_GROCERY_ITEM: 15,
    RECEIVE_NEW_GROCERY_ITEM: 16,
    DELETE_GROCERY_LIST: 17,
    EDIT_GROCERY_LIST: 18,
    DELETE_GROCERY_ITEM: 19,
    EDIT_GROCERY_ITEM: 20
};
Object.freeze(ActionType);  //Makes immutable

export default ActionType;