import InitialState from "../InitialState";
import ActionType from "../Actions/ActionType";

/**
 * The reducer used during the demo from the YouTube video I learned Redux from :)
 */
const demoReducer = (state = InitialState, action) => {
    
    switch (action.type) {
        case ActionType.INCREMENT:
            return {
                ...state,
                count: state.count + action.payload
            };
        case ActionType.DECREMENT:
            return {
                ...state,
                count: state.count - action.payload
            };
        default: 
            return state;
    }
};

export default demoReducer;