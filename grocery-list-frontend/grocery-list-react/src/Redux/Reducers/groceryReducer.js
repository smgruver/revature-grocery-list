import InitialState from "../InitialState";
import ActionType from "../Actions/ActionType";

/**
 * The reducer used during the demo from the YouTube video I learned Redux from :)
 */
const groceryReducer = (state = InitialState, action) => {

    switch (action.type) {
        case ActionType.INITIALIZE:
            return {
                ...state,
                groceryLists: action.payload,
                initialized: true,
            }
        case ActionType.SELECT_LIST:
            return {
                ...state,
                openedList: action.payload,
            };
        case ActionType.INPUT_GROCERY_LIST_NAME:
            return {
                ...state,
                newGroceryListNameInput: action.payload,
            };
        case ActionType.SUBMIT_NEW_GROCERY_LIST:
            return {
                ...state,
                waitingForServer: true,
                groceryListNameInput: "",
            };
        case ActionType.RECEIVE_NEW_GROCERY_LIST:

            return {
                ...state,
                waitingForServer: false,
                groceryLists: action.payload,
            };
        case ActionType.INPUT_GROCERY_ITEM_INFO:
            return {
                ...state,
                newGroceryItemInput: action.payload,
            };
        case ActionType.SUBMIT_NEW_GROCERY_ITEM:
            return {
                ...state,
                waitingForServer: true,
                groceryItemInput: {
                    itemName: "",
                    itemPrice: 0.00,
                    itemType: {
                        typeId: 0,
                        typeName: "",
                    }
                },
            };
        case ActionType.RECEIVE_NEW_GROCERY_ITEM:
            let updatedGroceryLists = [...state.groceryLists];    //clone groceryLists

            //Remove the old copy of openedList from groceryLists
            const index1 = state.groceryLists.indexOf(state.openedList);
            if (index1 > -1) {
                updatedGroceryLists.splice(index1, 1, action.payload);
            }

            return {
                ...state,
                waitingForServer: false,
                openedList: action.payload,
                groceryLists: updatedGroceryLists,
            }
        case ActionType.DELETE_GROCERY_LIST:
            let updatedGroceryListsAfterDelete = [...state.groceryLists];

            const index2 = state.groceryLists.indexOf(action.payload);
            if (index2 > -1) {
                updatedGroceryListsAfterDelete.splice(index2, 1);
            }

            return {
                ...state,
                groceryLists: updatedGroceryListsAfterDelete,
            }
        case ActionType.DELETE_GROCERY_ITEM:
            let updatedOpenedListAfterDeletedItem = {...state.openedList};
            let updatedGroceryListsAfterDeletedItem = [...state.groceryLists];

            const index3 = state.groceryLists.indexOf(state.openedList);
            if (index3 > -1) {
                updatedGroceryListsAfterDeletedItem.splice(index3, 1, updatedOpenedListAfterDeletedItem);
            }

            return {
                ...state,
                groceryLists: updatedGroceryListsAfterDeletedItem,
                openedList: updatedOpenedListAfterDeletedItem
            }
        default:
            return state;
    }
};

export default groceryReducer;