class GroceryListState {
    constructor(stateId, stateName) {
        this.stateId = stateId;
        this.stateName = stateName;
    }
}

export default GroceryListState;