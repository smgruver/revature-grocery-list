class GroceryItem {
    constructor(itemId, itemName, itemPrice, itemType) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.itemType = itemType;
    }
}

export default GroceryItem;