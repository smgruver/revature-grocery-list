class GroceryList {
    constructor(listId, listName, listState, items) {
        this.listId = listId;
        this.listName = listName;
        this.listState = listState;
        this.items = items;
    }
}

export default GroceryList;