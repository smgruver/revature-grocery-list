import React from "react";
import { useSelector, useDispatch } from "react-redux";
import ActionType from "../Redux/Actions/ActionType";
import Axios from "axios";
import GroceryList from "../Model/GroceryList";

/**
 * Displays all grocery lists in a table
 */
function GroceryListTable() {

    const myState = useSelector((state) => state);
    const dispatch = useDispatch();

    console.log(myState.groceryLists);

    /**
     * Sends and handles the axios request for deleting a Grocery List
     * @param {Event} event 
     * @param {GroceryList} groceryList 
     */
    let deleteRequest = (event, groceryList) => {

        Axios.delete("http://localhost:8080/grocery-list/" + groceryList.listId, {data: groceryList})
            .then((response) => {
                console.log(response.data);
                dispatch({type: ActionType.DELETE_GROCERY_LIST, payload: groceryList});
            })
            .catch((err) => console.log(err));
    };

    return (
        <table className="table table-hover">
            <thead className="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">List Name</th>
                    <th scope="col">List State</th>
                    <th scope="col">Item Count</th>
                    <th scope="col">Total Cost</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {myState.groceryLists.map((groceryList, index) => {

                    let totalPrice = 0;
                    if (groceryList.items && groceryList.items.length > 0) {
                        for (let tempItem of groceryList.items) {
                            totalPrice += tempItem.itemPrice;
                        }
                    }

                    return (
                        <tr key={"groceryList" + index}
                            onClick={() => dispatch({ type: ActionType.SELECT_LIST, payload: groceryList })}>
                            <td>{groceryList.listId}</td>
                            <td>{groceryList.listName}</td>
                            <td>{groceryList.stateId.stateName}</td>
                            <td>{(groceryList.items.length || 0)}</td>
                            <td>{totalPrice}</td>
                            <td>
                                <div className="form-group">
                                    <button className="btn btn-primary"
                                    >Edit</button>
                                </div>
                                <div className="form-group">
                                    <button className="btn btn-danger"
                                    onClick={(event, body) => deleteRequest(event, groceryList)}
                                    >Del</button>
                                </div>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}

export default GroceryListTable;