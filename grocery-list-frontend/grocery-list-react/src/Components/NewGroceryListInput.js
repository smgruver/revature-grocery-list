import React from "react";
import { useSelector, useDispatch } from "react-redux";
import ActionType from "../Redux/Actions/ActionType";
import Axios from "axios";

function NewGroceryListInput() {

    const myInput = useSelector((state) => state.newGroceryListNameInput);
    const myWaiting = useSelector((state) => state.waitingForServer);
    const dispatch = useDispatch();

    let submitNewGroceryList = (event) => {
        event.preventDefault();

        let body = {
            listName: myInput,
            stateId: {
                stateId: 1,
                stateName: "Ongoing",
            },
        }

        console.log(body);

        dispatch({type: ActionType.SUBMIT_NEW_GROCERY_LIST});

        Axios.post("http://localhost:8080/grocery-list", body)
            .then((response) => {
                console.log(response.data);
                dispatch({type: ActionType.RECEIVE_NEW_GROCERY_LIST, payload: response.data});
            })
            .catch((err) => console.log(err));
    }

    return (
        <form action="" className="form-inline" onSubmit={(event) => submitNewGroceryList(event)}>
            <div className="form-group">
                <input type="text" 
                className="form-control" 
                placeholder="Name" 
                required={true}
                value={myInput}
                onChange={(event) => 
                    dispatch({type: ActionType.INPUT_GROCERY_LIST_NAME, 
                        payload: event.target.value})}
                />
            </div>
            <div className="form-group">
                <button className="btn btn-success" 
                disabled={myWaiting}
                onSubmit={(event) => submitNewGroceryList(event)}>New</button>
            </div>
        </form>
    )
}

export default NewGroceryListInput;