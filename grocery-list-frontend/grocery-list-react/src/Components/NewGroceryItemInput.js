import React from "react";
import GroceryList from "../Model/GroceryList";
import { useSelector, useDispatch } from "react-redux";
import ActionType from "../Redux/Actions/ActionType";
import Axios from "axios";

/**
 * The form for inputting the information necessary to create a new Grocery Item; the name, price, and type
 */
function NewGroceryItemInput() {

    const myInput = useSelector((state) => state.newGroceryItemInput);
    const myWaiting = useSelector((state) => state.waitingForServer);
    const myOpenList = useSelector((state) => state.openedList);
    const dispatch = useDispatch();

    let handleFormInput = (event) => {
        switch(event.target.id) {
            case "itemNameInput":
                dispatch({type: ActionType.INPUT_GROCERY_ITEM_INFO, 
                    payload: {...myInput, itemName: event.target.value}});
                    break;
            case "itemPriceInput":
                dispatch({type: ActionType.INPUT_GROCERY_ITEM_INFO, 
                    payload: {...myInput, itemPrice: event.target.value}});
                break;
            case "itemTypeInput":
                let itemType;
                switch(event.target.value) {
                    case "Food":
                        itemType = {
                            typeId: 1,
                            typeName: "Food",
                        };
                        break;
                    case "Electronics":
                        itemType = {
                            typeId: 2,
                            typeName: "Electronics",
                        };
                        break;
                    case "Clothing":
                        itemType = {
                            typeId: 3,
                            typeName: "Clothing",
                        };
                        break;
                    case "Hardware":
                        itemType = {
                            typeId: 4,
                            typeName: "Hardware",
                        };
                        break;
                }

                dispatch({type: ActionType.INPUT_GROCERY_ITEM_INFO, 
                    payload: {...myInput, itemType: itemType}});
        }
    }

    let submitNewGroceryItem = (event) => {
        event.preventDefault();

        let body = {
            itemName: myInput.itemName,
            itemPrice: myInput.itemPrice,
            typeId: myInput.itemType,
            listId: myOpenList,
        }

        console.log(body);

        dispatch({type: ActionType.SUBMIT_NEW_GROCERY_ITEM});

        Axios.post("http://localhost:8080/grocery-list/" + myOpenList.listId + "/item", body)
            .then((response) => {
                console.log(response.data);
                dispatch({type: ActionType.RECEIVE_NEW_GROCERY_ITEM, payload: response.data});
            })
            .catch((err) => console.log(err));
    }

    return (
        <form action="" class="form-inline" onSubmit={(event) => submitNewGroceryItem(event)}>
            <div class="form-group">
                <input type="text" 
                id="itemNameInput"
                required={true}
                class="form-control" 
                placeholder="Name" 
                value={myInput.itemName}
                onChange={(event) => handleFormInput(event)}
                />
            </div>
            <div class="form-group">
                <input type="number" 
                id="itemPriceInput"
                class="form-control" 
                placeholder="0.00" 
                required={true}
                min="0.00"
                step=".01"
                value={myInput.itemPrice}
                onChange={(event) => handleFormInput(event)}
            />
            </div>
            <div class="form-group">
                <select class="form-control"
                id="itemTypeInput"
                required={true}
                value={myInput.itemType.typeName}
                onChange={(event) => handleFormInput(event)}>
                    <option value={null}>Item Type</option>
                    <option value="Food">Food</option>
                    <option value="Electronics">Electronics</option>
                    <option value="Clothing">Clothing</option>
                    <option value="Hardware">Hardware</option>
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-success" 
                disabled={myWaiting || !myInput.itemName || !myInput.itemType || !myInput.itemPrice}
                onSubmit={(event) => submitNewGroceryItem(event)}>New</button>
            </div>
        </form>
    )
}

export default NewGroceryItemInput;