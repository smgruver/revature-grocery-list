import React from "react";
import { useSelector, useDispatch } from "react-redux";
import ActionType from "../Redux/Actions/ActionType";
import Axios from "axios";

/**
 * Displays the GroceryItems stored inside of the selected ("opened") Grocery List
 */
function GroceryItemTable() {

    const myOpenList = useSelector((state) => state.openedList);
    const myGroceryLists = useSelector((state) => state.groceryLists);
    const myWaiting = useSelector((state) => state.waitingForServer);
    const dispatch = useDispatch();

    let deleteRequest = (event, groceryItem) => {
        console.log(groceryItem);

        Axios.delete("http://localhost:8080/grocery-list/" + myOpenList.listId + "/item", {data: groceryItem})
            .then((response) => {
                console.log(response.data);
                dispatch({type: ActionType.DELETE_GROCERY_ITEM, payload: groceryItem});
            })
            .catch((err) => console.log(err));
    };

    if (myOpenList) {
        return (

            <table class="table table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Item Id#</th>
                        <th scope="col">Item Name</th>
                        <th scope="col">Item Price</th>
                        <th scope="col">Item Type</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    {(myOpenList.items ? myOpenList.items.map((groceryItem, index) => {
                        return (
                            <tr key={"groceryList" + index}>
                                <td>{groceryItem.itemId}</td>
                                <td>{groceryItem.itemName}</td>
                                <td>{groceryItem.itemPrice}</td>
                                <td>{groceryItem.typeId.typeName}</td>
                                <td>
                                    <div className="form-group">
                                        <button className="btn btn-primary"
                                        disabled={myWaiting}
                                            >Edit</button>
                                    </div>
                                    <div className="form-group">
                                        <button className="btn btn-danger"
                                        disabled={myWaiting}
                                        onClick={(event, body) => deleteRequest(event, groceryItem)}
                                            >Del</button>
                                    </div>
                                </td>
                            </tr>
                        );
                    }) : <React.Fragment></React.Fragment>)}
                </tbody>
            </table>
        );
    } else {
        return (
            <h1>Click a Grocery List to see its contents</h1>
        );
    }
};

export default GroceryItemTable;