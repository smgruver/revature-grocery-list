/**
 * 
 */
package com.grocerylist.training.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grocerylist.training.model.GroceryItem;
import com.grocerylist.training.model.GroceryList;
import com.grocerylist.training.model.GroceryListState;
import com.grocerylist.training.serv.GroceryListService;

/**
 * Controller handling HTTP requests to and responses from endpoints associated with GroceryList entities
 * @author sgruv
 *
 */
@RestController
@RequestMapping("/grocery-list")
public class GroceryListController {

	private GroceryListService listServ;
	
	public GroceryListController() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * All args constructor for constructor injection of dependencies
	 * @param listServ
	 */
	public GroceryListController(GroceryListService listServ) {
		super();
		this.listServ = listServ;
	}
	
	@GetMapping
	public ResponseEntity<List<GroceryList>> getAllGroceryLists() {
		List<GroceryList> tempList = new ArrayList<>();
		GroceryList tempGroceryList = new GroceryList(1, "tempName", new GroceryListState("Ongoing"));
		tempList.add(tempGroceryList);
		
		return new ResponseEntity<List<GroceryList>>(tempList, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<String> addGroceryList(@RequestBody GroceryList newList) {
		return new ResponseEntity<String>("" + newList.getListName(), HttpStatus.OK);
	}

	@GetMapping(value = {"/{listId}/item", "/{listId}"})
	public ResponseEntity<GroceryList> getGroceryList(@PathVariable("listId") int listId) {
		GroceryList tempGroceryList = new GroceryList(1, "tempName", new GroceryListState("Ongoing"));
		
		return new ResponseEntity<GroceryList>(tempGroceryList, HttpStatus.OK);
	}
	
	@PostMapping(value = {"/{listId}/item", "/{listId}"})
	public ResponseEntity<String> addItemToGroceryList
									(@PathVariable("listId") int listId, 
									@RequestBody GroceryItem newItem) {
		GroceryList tempGroceryList = new GroceryList(listId, "tempName", new GroceryListState("Ongoing"));
		tempGroceryList.getItems().add(newItem);
		
		return new ResponseEntity<String>("" + listId + " " + tempGroceryList.getItems().get(0).getItemName(), HttpStatus.OK);
	}
	
	@PutMapping(value = "/{listId}")
	public ResponseEntity<String> updateGroceryList
									(@PathVariable("listId") int listId, 
									@RequestBody GroceryList toGroceryList) {
		
		GroceryList tempGroceryList = toGroceryList;
		
		return new ResponseEntity<String>("" + listId + " " + tempGroceryList.getListName(), HttpStatus.OK);
	}
	
	@PutMapping(value = "/{listId}/item/{itemId}")
	public ResponseEntity<String> updateItemFromListWithPathVariable
									(@PathVariable("listId") int listId, 
									@PathVariable("itemId") int itemId, 
									@RequestBody GroceryItem toGroceryItem) {
		
		return new ResponseEntity<String>("" + listId + " " + itemId + " " + toGroceryItem.getItemName(), HttpStatus.OK);
	}
	
	@PutMapping(value = {"/{listId}/item"})
	public ResponseEntity<String> updateItemFromListWithoutPathVariable
									(@PathVariable("listId") int listId,  
									@RequestBody GroceryItem toGroceryItem) {
		
		return new ResponseEntity<String>("" + listId + " " + toGroceryItem.getItemName(), HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{listId}")
	public ResponseEntity<String> deleteGroceryList(@PathVariable("listId") int listId) {
		
		return new ResponseEntity<String>("tempMessage", HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{listId}/item/{itemId}")
	public ResponseEntity<String> deleteGroceryListItemWithPathVariable
									(@PathVariable("listId") int listId,
									@PathVariable("itemId") int itemId) {
		
		return new ResponseEntity<String>("" + listId + " " + itemId, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{listId}/item")
	public ResponseEntity<String> deleteGroceryListItemWithoutPathVariable
									(@PathVariable("listId") int listId,
									@RequestBody GroceryItem toDelete) {
		
		return new ResponseEntity<String>("" + listId + " " + toDelete.getItemName(), HttpStatus.OK);
	}
}
