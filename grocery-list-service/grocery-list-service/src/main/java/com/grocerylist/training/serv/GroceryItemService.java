/**
 * 
 */
package com.grocerylist.training.serv;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grocerylist.training.model.GroceryItem;
import com.grocerylist.training.repo.GroceryItemRepo;

/**
 * Service bean for accessing entities of type GroceryItem
 * @author sgruv
 *
 */
@Service
public class GroceryItemService {

	private GroceryItemRepo itemRepo;
	
	public GroceryItemService() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * All args constructor for constructor injection of dependencies
	 * @param itemRepo
	 */
	@Autowired
	public GroceryItemService(GroceryItemRepo itemRepo) {
		super();
		this.itemRepo = itemRepo;
	}
	
	public GroceryItem getGroceryItem(int itemId) {
		Optional<GroceryItem> target = itemRepo.findById(itemId);
		
		if (target.isPresent()) {
			return target.get();
		} else {
			return null;
		}
	}
	
	public void updateGroceryItem(GroceryItem toGroceryItem) {
		itemRepo.save(toGroceryItem);
	}
}
