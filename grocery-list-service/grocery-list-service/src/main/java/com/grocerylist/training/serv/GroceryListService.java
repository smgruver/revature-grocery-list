/**
 * 
 */
package com.grocerylist.training.serv;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grocerylist.training.model.GroceryItem;
import com.grocerylist.training.model.GroceryList;
import com.grocerylist.training.repo.GroceryListRepo;

/**
 * Service bean for accessing entities of type GroceryList
 * @author sgruv
 *
 */
@Service
public class GroceryListService {

	private GroceryListRepo listRepo;
	private GroceryItemService itemServ;
	
	public GroceryListService() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * All args constructor for constructor injection of dependencies
	 * @param listRepo
	 */
	@Autowired
	public GroceryListService(GroceryListRepo listRepo, GroceryItemService itemServ) {
		super();
		this.listRepo = listRepo;
		this.itemServ = itemServ;
	}
	
	public List<GroceryList> getAllGroceryLists() {
		return listRepo.findAll();
	}
	
	public void addGroceryList(GroceryList newGroceryList) {
		listRepo.save(newGroceryList);
	}
	
	public GroceryList getGroceryListById(int listId) {
		Optional<GroceryList> target = listRepo.findById(listId);
		
		if (target.isPresent()) {
			return target.get();
		} else 
			return null;
		
	}
	
	public void addGroceryItemToGroceryList(int listId, GroceryItem newitem) {
		GroceryList target = getGroceryListById(listId);
		
		if (target != null) {
			target.getItems().add(newitem);
			listRepo.save(target);
		} else {
			return;
		}
	}
	
	public void updateGroceryList(GroceryList toGroceryList, int listId) {
		GroceryList target = getGroceryListById(listId);
		
		if (target != null) {
			target.setListName(toGroceryList.getListName());
			listRepo.save(target);
		} else {
			return;
		}
	}
	
	public void updateItemFromListWithPathVariable(int listId, int itemId, GroceryItem toGroceryItem) {
		GroceryItem targetItem = itemServ.getGroceryItem(itemId);
		
		if (listId == targetItem.getListId().getListId()) {
			
			
			
		}
	}
}
