package com.grocerylist.training.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.grocerylist.training.model.GroceryItem;

/**
 * JpaRepository for accessing GroceryItem entity
 * @author sgruv
 *
 */
@Repository
public interface GroceryItemRepo extends JpaRepository<GroceryItem, Integer> {

}
