package com.grocerylist.training.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "GroceryItem")
public class GroceryItem {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "itemId")
	private int itemId;
	
	@Column(name = "itemName", length = 32, nullable = false)
	private String itemName;
	
	@Column(name = "itemPrice", nullable = false, precision = 10, scale = 2)
	private double itemPrice;
	
	@ManyToOne
	@JoinColumn(name = "typeId", nullable = false)
	private GroceryItemType typeId;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "listId", nullable = false)
	@JsonIgnore
	private GroceryList listId;

	/**
	 * No args constructor
	 */
	public GroceryItem() {
		super();
	}
	
	/**
	 * Non-generated fields with GroceryItemType foreign key reference, without GroceryList foriegn key reference
	 * @param itemName
	 * @param itemPrice
	 * @param typeId
	 */
	public GroceryItem(String itemName, double itemPrice, GroceryItemType typeId) {
		super();
		this.itemName = itemName;
		this.itemPrice = itemPrice;
		this.typeId = typeId;
	}

	/**
	 * Non-generated fields with GroceryList and GroceryItemType foreign key references
	 * @param itemName
	 * @param itemPrice
	 * @param typeId
	 * @param listId
	 */
	public GroceryItem(String itemName, double itemPrice, GroceryItemType typeId, GroceryList listId) {
		super();
		this.itemName = itemName;
		this.itemPrice = itemPrice;
		this.typeId = typeId;
		this.listId = listId;
	}
	
	

	/**
	 * All args constructor
	 * @param itemId
	 * @param itemName
	 * @param itemPrice
	 * @param typeId
	 * @param listId
	 */
	public GroceryItem(int itemId, String itemName, double itemPrice, GroceryItemType typeId, GroceryList listId) {
		super();
		this.itemId = itemId;
		this.itemName = itemName;
		this.itemPrice = itemPrice;
		this.typeId = typeId;
		this.listId = listId;
	}

	/**
	 * @return the itemId
	 */
	public int getItemId() {
		return itemId;
	}

	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}

	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	/**
	 * @return the itemPrice
	 */
	public double getItemPrice() {
		return itemPrice;
	}

	/**
	 * @param itemPrice the itemPrice to set
	 */
	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	/**
	 * @return the typeId
	 */
	public GroceryItemType getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(GroceryItemType typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the listId
	 */
	public GroceryList getListId() {
		return listId;
	}

	/**
	 * @param listId the listId to set
	 */
	public void setListId(GroceryList listId) {
		this.listId = listId;
	}

	@Override
	public String toString() {
		return "GroceryItem [itemId=" + itemId + ", itemName=" + itemName + ", itemPrice=" + itemPrice + ", typeId="
				+ typeId + ", listId=" + listId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + itemId;
		result = prime * result + ((itemName == null) ? 0 : itemName.hashCode());
		long temp;
		temp = Double.doubleToLongBits(itemPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((listId == null) ? 0 : listId.hashCode());
		result = prime * result + ((typeId == null) ? 0 : typeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroceryItem other = (GroceryItem) obj;
		if (itemId != other.itemId)
			return false;
		if (itemName == null) {
			if (other.itemName != null)
				return false;
		} else if (!itemName.equals(other.itemName))
			return false;
		if (Double.doubleToLongBits(itemPrice) != Double.doubleToLongBits(other.itemPrice))
			return false;
		if (listId == null) {
			if (other.listId != null)
				return false;
		} else if (!listId.equals(other.listId))
			return false;
		if (typeId == null) {
			if (other.typeId != null)
				return false;
		} else if (!typeId.equals(other.typeId))
			return false;
		return true;
	}
	
}
