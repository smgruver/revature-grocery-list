package com.grocerylist.training.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Represents the lookup table for grocery items' types.
 * @author sgruv
 *
 */
@Entity
@Table(name = "GroceryItemType")
public class GroceryItemType {
	
	@Id
	@Column(name = "typeId", length = 16)
	private String typeId;
	
	/**
	 * No args constructor
	 */
	public GroceryItemType() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Non-generated args constructor / All args constructor
	 * @param typeName
	 */
	public GroceryItemType(String typeId) {
		super();
		this.typeId = typeId;
	}

	/**
	 * @return the typeId
	 */
	public String getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	
	@Override
	public String toString() {
		return "GroceryItemType [typeId=" + typeId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((typeId == null) ? 0 : typeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroceryItemType other = (GroceryItemType) obj;
		if (typeId == null) {
			if (other.typeId != null)
				return false;
		} else if (!typeId.equals(other.typeId))
			return false;
		return true;
	}	
}
