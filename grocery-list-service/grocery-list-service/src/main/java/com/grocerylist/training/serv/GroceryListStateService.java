/**
 * 
 */
package com.grocerylist.training.serv;

import org.springframework.stereotype.Service;

import com.grocerylist.training.repo.GroceryListStateRepo;

/**
 * Service bean for accessing entities of type GroceryListState
 * @author sgruv
 *
 */
@Service
public class GroceryListStateService {

	private GroceryListStateRepo stateRepo;
	
	public GroceryListStateService() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * All args constructor for constructor injection of dependencies
	 * @param stateRepo
	 */
	public GroceryListStateService(GroceryListStateRepo stateRepo) {
		super();
		this.stateRepo = stateRepo;
	}
	
}
