package com.grocerylist.training.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.grocerylist.training.model.GroceryItemType;

/**
 * JpaRepository for accessing GroceryItemType entities
 * @author sgruv
 *
 */
@Repository
public interface GroceryItemTypeRepo extends JpaRepository<GroceryItemType, String> {

}
