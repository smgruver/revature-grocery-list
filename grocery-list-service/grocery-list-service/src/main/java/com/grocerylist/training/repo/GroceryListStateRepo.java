package com.grocerylist.training.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.grocerylist.training.model.GroceryListState;

/**
 * JpaRepository for accessing GroceryListState entities
 * @author sgruv
 *
 */
@Repository
public interface GroceryListStateRepo extends JpaRepository<GroceryListState, String> {

}
