package com.grocerylist.training.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "GroceryList")
public class GroceryList {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "listId")
	private int listId;
	
	@Column(name = "listName", length = 32)
	private String listName;
	
	@ManyToOne
	@JoinColumn(name = "stateId", nullable = false)
	private GroceryListState stateId;
	
	@OneToMany(mappedBy = "listId", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<GroceryItem> items = new ArrayList<>();
	
	/**
	 * No args constructor
	 */
	public GroceryList() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Non-generated fields constructor
	 * @param listName
	 * @param stateId
	 */
	public GroceryList(String listName, GroceryListState stateId) {
		super();
		this.listName = listName;
		this.stateId = stateId;
	}

	/**
	 * All args constructor, default empty list of items
	 * @param listId
	 * @param listName
	 * @param stateId
	 */
	public GroceryList(int listId, String listName, GroceryListState stateId) {
		super();
		this.listId = listId;
		this.listName = listName;
		this.stateId = stateId;
	}
	
	/**
	 * All args constructor with pre-existing list of GroceryItems
	 * @param listId
	 * @param listName
	 * @param stateId
	 * @param items
	 */
	public GroceryList(int listId, String listName, GroceryListState stateId, List<GroceryItem> items) {
		super();
		this.listId = listId;
		this.listName = listName;
		this.stateId = stateId;
		this.items = items;
	}

	/**
	 * @return the listId
	 */
	public int getListId() {
		return listId;
	}

	/**
	 * @param listId the listId to set
	 */
	public void setListId(int listId) {
		this.listId = listId;
	}

	/**
	 * @return the listName
	 */
	public String getListName() {
		return listName;
	}

	/**
	 * @param listName the listName to set
	 */
	public void setListName(String listName) {
		this.listName = listName;
	}

	/**
	 * @return the stateId
	 */
	public GroceryListState getStateId() {
		return stateId;
	}

	/**
	 * @param stateId the stateId to set
	 */
	public void setStateId(GroceryListState stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the items
	 */
	public List<GroceryItem> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(List<GroceryItem> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "GroceryList [listId=" + listId + ", listName=" + listName + ", stateId=" + stateId + ", items=" + items
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + listId;
		result = prime * result + ((listName == null) ? 0 : listName.hashCode());
		result = prime * result + ((stateId == null) ? 0 : stateId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroceryList other = (GroceryList) obj;
		if (listId != other.listId)
			return false;
		if (listName == null) {
			if (other.listName != null)
				return false;
		} else if (!listName.equals(other.listName))
			return false;
		if (stateId == null) {
			if (other.stateId != null)
				return false;
		} else if (!stateId.equals(other.stateId))
			return false;
		return true;
	}
}
