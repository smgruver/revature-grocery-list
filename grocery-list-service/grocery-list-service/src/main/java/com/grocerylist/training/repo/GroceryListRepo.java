package com.grocerylist.training.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.grocerylist.training.model.GroceryList;

/**
 * JpaRepository for accessing GroceryList entities
 * @author sgruv
 *
 */
@Repository
public interface GroceryListRepo extends JpaRepository<GroceryList, Integer> {

}
