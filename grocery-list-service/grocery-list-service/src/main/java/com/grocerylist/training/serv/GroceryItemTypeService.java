/**
 * 
 */
package com.grocerylist.training.serv;

import org.springframework.stereotype.Service;

import com.grocerylist.training.repo.GroceryItemTypeRepo;

/**
 * Service bean for accessing entities of type GroceryItemType
 * @author sgruv
 *
 */
@Service
public class GroceryItemTypeService {

	private GroceryItemTypeRepo typeRepo;
	
	public GroceryItemTypeService() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * All args constructor for constructor injection of dependencies
	 * @param typeRepo
	 */
	public GroceryItemTypeService(GroceryItemTypeRepo typeRepo) {
		super();
		this.typeRepo = typeRepo;
	}
	
	
}
