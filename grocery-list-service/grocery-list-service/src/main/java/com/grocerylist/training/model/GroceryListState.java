package com.grocerylist.training.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Represents the lookup table for different states of a grocery list
 * @author sgruv
 *
 */
@Entity
@Table(name="GroceryListState")
public class GroceryListState {

	@Id	
	@Column(name = "stateId", length = 16)
	private String stateId;
	
	/**
	 * No args constructor
	 */
	public GroceryListState() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Non-generated args constructor / All args constructor
	 * @param stateName
	 */
	public GroceryListState(String stateId) {
		super();
		this.stateId = stateId;
	}

	/**
	 * @return the stateId
	 */
	public String getStateId() {
		return stateId;
	}

	/**
	 * @param stateId the stateId to set
	 */
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	
	@Override
	public String toString() {
		return "GroceryListState [stateId=" + stateId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((stateId == null) ? 0 : stateId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroceryListState other = (GroceryListState) obj;
		if (stateId == null) {
			if (other.stateId != null)
				return false;
		} else if (!stateId.equals(other.stateId))
			return false;
		return true;
	}
}
